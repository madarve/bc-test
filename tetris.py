import random
import copy
import sys

BOARD_HEIGHT = 22
BOARD_WIDTH = 22


class TetrisPiece(object):
    def __init__(self, name, states, centroid, current_state=None, is_active=True):
        self.name = name
        self.states = states
        self.centroid = centroid
        self.is_active = is_active
        if current_state:
            self.current_state = current_state
        else:
            self.current_state = random.randint(0, len(self.states) - 1)

    @staticmethod
    def factory(piece_type):
        available_types = {
            "PieceI": PieceI(),
            "PieceO": PieceO(),
            "PieceL": PieceL(),
            "PieceJ": PieceJ(),
            "PieceS": PieceS()
        }

        return available_types[piece_type]

    def move_down(self):
        """All pieces move down when rotating or moving"""
        new_centroid = (self.centroid[0], self.centroid[1] + 1)
        self.centroid = new_centroid

    def rotate_clockwise(self):
        if self.current_state == len(self.states) - 1:
            self.current_state = 0
        else:
            self.current_state += 1
        self.move_down()

    def rotate_counter_clockwise(self):
        if self.current_state == 0:
            self.current_state = len(self.states) - 1
        else:
            self.current_state -= 1
        self.move_down()

    def _move(self, direction):
        if direction == 'right':
            x_pos = 1
        elif direction == 'left':
            x_pos = -1
        else:
            raise NotImplementedError

        # Update centroid position
        self.centroid = (self.centroid[0] + x_pos, self.centroid[1])
        self.move_down()

    def move_left(self):
        self._move('left')

    def move_right(self):
        self._move('right')


class PieceI(TetrisPiece):
    def __init__(self):
        states = [[(0, 0), (0, 1), (0, 2), (0, 3)],
                  [(0, 0), (1, 0), (2, 0), (3, 0)]]

        TetrisPiece.__init__(self, name="I", states=states, centroid=(0, 0))

    def _rotate(self):
        """For this figure, rotation is the same in all directions"""
        self.current_state = 1 if self.current_state == 0 else 0
        self.move_down()

    def rotate_counter_clockwise(self):
        self._rotate()

    def rotate_clockwise(self):
        self._rotate()


class PieceO(TetrisPiece):
    def __init__(self):
        states = [[(0, 0), (0, 1), (1, 0), (1, 1)]]
        TetrisPiece.__init__(self, name="O", states=states, centroid=(0, 0))

    def rotate_counter_clockwise(self):
        self.move_down()

    def rotate_clockwise(self):
        self.move_down()


class PieceS(TetrisPiece):
    def __init__(self):
        states = [[(0, 0), (0, 1), (1, 1), (1, 2)],
                  [(0, 0), (1, 0), (1, 1), (2, 1)]]

        TetrisPiece.__init__(self, name="S", states=states, centroid=(0, 0),)


class PieceJ(TetrisPiece):
    def __init__(self):

        states = [[(1, 0), (1, 1), (1, 2), (0, 2)],
                  [(0, 0), (0, 1), (1, 1), (2, 1)],
                  [(0, 0), (1, 0), (0, 1), (0, 2)],
                  [(0, 0), (1, 0), (2, 0), (2, 1)]]

        TetrisPiece.__init__(self, name="J", states=states, centroid=(0, 0))


class PieceL(TetrisPiece):
    def __init__(self):
        states = [[(0, 0), (0, 1), (0, 2), (1, 2)],
                        [(0, 1), (1, 1), (2, 1), (2, 0)],
                        [(0, 0), (1, 0), (1, 1), (1, 2)],
                        [(0, 1), (0, 0), (1, 0), (2, 0)]]

        TetrisPiece.__init__(self, name="L", states=states, centroid=(0, 0))


class GameBoard:
    blank_board = [[1 if y == BOARD_HEIGHT-1 or (x == 0 or x == BOARD_WIDTH-1)
                    else 0 for x in xrange(BOARD_WIDTH)]
                    for y in xrange(BOARD_HEIGHT)]

    def __init__(self):
        self.width = BOARD_WIDTH
        self.height = BOARD_HEIGHT
        self.is_first_move = True
        self.state = copy.deepcopy(GameBoard.blank_board)
        self.current_pieces = []
        self.active_piece = None

    def update_pieces(self):
        # clear previous state:
        self.state = copy.deepcopy(GameBoard.blank_board)

        # Update with new states
        for piece in self.current_pieces:
            for x, y in piece.states[piece.current_state]:
                new_x = x + piece.centroid[0]
                new_y = y + piece.centroid[1]

                if piece.is_active:
                    if self.state[new_y][new_x] == 1:  # Collision detected
                        piece.is_active = False
                        self.active_piece = None
                        break
                    else:
                        self.state[new_y][new_x] = 1
                else:
                    self.state[piece.centroid[0]][piece.centroid[1]] = 1

    def generate_piece(self):
        """Generates a random piece to appear on the top of the board"""
        types = TetrisPiece.__subclasses__()
        piece_type = random.choice(types).__name__
        piece = TetrisPiece.factory(piece_type)
        self.current_pieces.append(piece)

        # Select a random point on the board for a piece to appear:
        x_init = random.randint(4, self.width-4)
        piece.centroid = (x_init, 0)

        self.active_piece = piece
        piece.is_active = True

        return piece

    def play(self):
        """(Re)draws the whole game board according to current game state"""
        if self.is_first_move:
            self.is_first_move = False
            self.generate_piece()
            self.update_pieces()
        else:
            if not self.active_piece:
                self.generate_piece()
            next_move = get_move()
            getattr(self.active_piece, next_move)()
            self.update_pieces()

        print_board(self.state)


def get_move():
    """Prompts the user to make a move and returns the move command"""
    available_moves = {
        'w': 'rotate_counter_clockwise',
        's': 'rotate_clockwise',
        'a': 'move_left',
        'd': 'move_right'
    }

    move_command = raw_input("Next move (W,A,S,D) or 'q' to exit: ")
    if move_command == 'q':
        print "bye bye"
        sys.exit(0)

    elif move_command in available_moves.keys():
        return available_moves[move_command]

    else:
        print "Invalid input, try again..."
        return get_move()


def print_board(board):
    print "\n----------------- TETRIS! -----------------\n\n"
    print('\n'.join([''.join(['{:2}'.format("*" if item else " ") for item in row]) 
      for row in board]))
    print "\n-------------------------------------------\n\n"


def game_loop():
    """This is the main game loop, it executes game logic 
    until game is over or until the user exits"""
    game = GameBoard()
    while True:
        game.play()

if __name__ == '__main__':
    game_loop()
